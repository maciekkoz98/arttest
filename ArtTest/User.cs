﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtTest
{
    public class User
    {
        public string UserName { get; }
        public string Answers { get; set; }
        public List<string> AnswersList { get; set; }
        public int Score { get; set; }

        private readonly List<string> correctAnswers;

        public int ID { get; set; }

        public User(string name)
        {
            UserName = name;
            AnswersList = new List<string>(32);
            for (int i = 0; i < 32; i++)
            {
                AnswersList.Add(null);
            }
            correctAnswers = new List<string>();
            SetupCorrectAnswers();           
        }

        public User() { }

        public void AddAnswer(int index, string answer) => AnswersList[index] = answer;

        public string GetAnswer(int index) => AnswersList.ElementAt(index);

        public void Export()
        {
            DataBaseConnection.AddUser(this);
        }

        public void CountScore()
        {          
            for (int i = 0; i < 32; i++)
            {
                if (AnswersList[i] != null && AnswersList[i].Equals(correctAnswers[i]))
                {
                    Score++;
                }
            }
        }

        private void SetupCorrectAnswers()
        {
            string answers = "b c a b c a b c a b c a a a a b b a b a c b c c a b c a a b a b";
            string[] answersTab = answers.Split(' ');
            for (int i = 0; i < 32; i++)
            {
                correctAnswers.Add(answersTab[i]);
            }
        }

        public string GetAnswers()
        {
            StringBuilder b = new StringBuilder();
            foreach (string ans in AnswersList)
            {
                b.Append(ans).Append(" ");
            }
            return b.ToString();
        }

        public static int ScoreComparison(User u1, User u2)
        {
            return u2.Score.CompareTo(u1.Score);
        }

    }
}
