﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ArtTest
{
    /// <summary>
    /// Logika interakcji dla klasy QuestionPage.xaml
    /// </summary>
    public partial class QuestionPage : Page
    {
        private readonly Frame main;
        private readonly User currentUser;
        private int questionIndex;

        private Storyboard movingPictures;
        private string imagesPageButtonClicked;
        private bool clickedBeforePrevious;

        public QuestionPage(Frame main, string userName)
        {
            InitializeComponent();
            this.main = main;
            currentUser = new User(userName);
            userNameLabel.Text = userName;
            questionIndex = 1;
        }

        public void BackToMainMenu(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Czy na pewno chcesz wyjść?" + Environment.NewLine + "Postęp nie zostanie zapisany!", "ArtTest", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                FadeOutAllComponents("mainMenu");
            }
        }

        private void NextPage(object sender, EventArgs e)
        {
            ImagesPage currentImages = (ImagesPage)imagesFrame.Content;
            if (currentImages.GetButtonClicked() != null)
                imagesPageButtonClicked = currentImages.GetButtonClicked();
            currentUser.AddAnswer(questionIndex - 1, imagesPageButtonClicked);
            if (questionIndex < 32)
            {
                movingPictures = currentImages.Resources["leftOut"] as Storyboard;
                movingPictures.Completed += NextQuestionPage;
                movingPictures.Begin();
            }
            else if (questionIndex == 32)
            {
                MessageBoxResult result = MessageBox.Show("Czy chcesz zakończyć test?", "ArtTest", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    FadeOutAllComponents("sumUpPage");
                }
            }
        }

        private void NextQuestionPage(object sender, EventArgs e)
        {
            ImagesPage nextImages = new ImagesPage(++questionIndex, true, this);
            imagesFrame.Content = nextImages;
            SetTexts();
            movingPictures.Completed -= NextQuestionPage;
            movingPictures = nextImages.Resources["leftIn"] as Storyboard;
            if (currentUser.GetAnswer(questionIndex - 1) != null)
            {
                if (imagesPageButtonClicked.Equals(currentUser.GetAnswer(questionIndex - 1)))
                {
                    nextImages.SetAnswerLabelUp(imagesPageButtonClicked);
                }
                else
                {
                    nextImages.ShowAnswerLabel(currentUser.GetAnswer(questionIndex - 1));
                    nextImages.SetStoryboard("labelDown", imagesPageButtonClicked);

                }
            }
            else
            {
                nextButton.IsEnabled = false;
                nextImages.SetStoryboard("labelDown", imagesPageButtonClicked);

            }
            movingPictures.Begin();
        }

        private void PreviousPage(object sender, EventArgs e)
        {
            if (questionIndex > 1)
            {
                ImagesPage currentImages = (ImagesPage)imagesFrame.Content;
                if (currentImages.GetButtonClicked() != null)
                {
                    imagesPageButtonClicked = currentImages.GetButtonClicked();
                    clickedBeforePrevious = true;
                }
                else
                {
                    clickedBeforePrevious = false;
                }
                movingPictures = currentImages.Resources["rightOut"] as Storyboard;
                movingPictures.Completed += PreviousQuestionPage;
                movingPictures.Begin();
            }
        }

        private void PreviousQuestionPage(object sender, EventArgs e)
        {
            ImagesPage previousImages = new ImagesPage(--questionIndex, false, this);
            imagesFrame.Content = previousImages;
            SetTexts();
            movingPictures.Completed -= PreviousQuestionPage;
            movingPictures = previousImages.Resources["rightIn"] as Storyboard;
            movingPictures.Begin();
            if (imagesPageButtonClicked.Equals(currentUser.GetAnswer(questionIndex - 1)))
            {
                if (clickedBeforePrevious)
                    previousImages.SetAnswerLabelUp(imagesPageButtonClicked);
                else
                    previousImages.ShowAnswerLabel(currentUser.GetAnswer(questionIndex - 1));
            }
            else
            {
                previousImages.SetStoryboard("labelDown", imagesPageButtonClicked);
                previousImages.ShowAnswerLabel(currentUser.GetAnswer(questionIndex - 1));
            }
            nextButton.IsEnabled = true;
        }

        private void SetTexts()
        {
            if (questionIndex >= 22)
            {
                partNumberText.Text = "II";
                questionRestText.Text = "/11";
                questionNumberText.Text = (questionIndex - 21).ToString();
            }
            else
            {
                partNumberText.Text = "I";
                questionRestText.Text = "/21";
                questionNumberText.Text = questionIndex.ToString();
            }
        }

        private void SetImages(object sender, EventArgs e)
        {
            imagesFrame.Content = new ImagesPage(this);
        }

        private void ChangeToMainMenu(object sender, EventArgs e)
        {
            main.Content = new MainMenu(main);
        }

        private void ChangeToSumUp(object sender, EventArgs e)
        {
            currentUser.CountScore();
            main.Content = new SumUpPage(main, currentUser);
        }

        private void FadeOutAllComponents(string pageName)
        {
            DoubleAnimation fadeOut = new DoubleAnimation(1.0, 0.0, new Duration(TimeSpan.FromMilliseconds(250)));
            if (pageName == "mainMenu")
            {
                fadeOut.Completed += new EventHandler(ChangeToMainMenu);
            }
            else if (pageName == "sumUpPage")
            {
                fadeOut.Completed += new EventHandler(ChangeToSumUp);
            }
            mainStackPanel.BeginAnimation(OpacityProperty, fadeOut);
        }

        public void NextActive() => nextButton.IsEnabled = true;
    }
}
