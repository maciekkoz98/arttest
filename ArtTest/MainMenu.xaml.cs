﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ArtTest
{
    /// <summary>
    /// Logika interakcji dla klasy MainMenu.xaml
    /// </summary>
    public partial class MainMenu : Page
    {
        private readonly Frame main;
        private StackPanel userPhotoPanel;
        private TextBox enterUserName;
        private readonly DoubleAnimation fadeOut = new DoubleAnimation(1.0, 0.0, new Duration(TimeSpan.FromMilliseconds(250)));
        private readonly DoubleAnimation fadeIn = new DoubleAnimation(0.0, 1.0, new Duration(TimeSpan.FromMilliseconds(250)));

        public MainMenu(Frame main)
        {
            InitializeComponent();
            this.main = main;
        }

        private void StartQuiz(object sender, RoutedEventArgs e)
        {
            if (enterUserName.Text == "")
            {
                MessageBox.Show("Należy podać imię użytkownika!", "ArtTest", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            else if (enterUserName.Text.Count() > 12)
            {
                MessageBox.Show("Podane imię jest za długie (do 12 znaków)", "ArtTest", MessageBoxButton.OK, MessageBoxImage.Warning);
                enterUserName.Text = "";
                return;
            }

            DoubleAnimation fadeOutAndDo1 = new DoubleAnimation(1.0, 0.0, new Duration(TimeSpan.FromMilliseconds(250)));
            fadeOutAndDo1.Completed += new EventHandler(ChangePage);
            foreach (FrameworkElement child in MainPanel.Children)
            {
                if (child != null)
                {
                    child.BeginAnimation(OpacityProperty, fadeOutAndDo1);
                }
            }
        }

        private void GetClientName(object sender, RoutedEventArgs e)
        {
            DoubleAnimation fadeOutAndDo = new DoubleAnimation(1.0, 0.0, new Duration(TimeSpan.FromMilliseconds(250)));
            fadeOutAndDo.Completed += new EventHandler(ChangePanels);

            startButton.BeginAnimation(OpacityProperty, fadeOut);
            helpButton.BeginAnimation(OpacityProperty, fadeOut);
            resultButton.BeginAnimation(OpacityProperty, fadeOutAndDo);
        }

        private void ChangePanels(object sender, EventArgs e)
        {
            userPhotoPanel = new StackPanel();
            Image userAvatar = new Image
            {
                Height = 150,
                Source = new BitmapImage(new Uri("pack://application:,,,/Images/user_proto.jpg")),
                Stretch = Stretch.Uniform,
                Margin = new Thickness(0, 0, 0, 25)
            };
            StackPanel userNamePanel = new StackPanel
            {
                Name = "userNamePanel",
                Orientation = 0
            };
            TextBlock userLabel = new TextBlock
            {
                FontSize = 22,
                Text = "Imię: ",
                Margin = new Thickness(80, 0, 50, 50)
            };
            enterUserName = new TextBox
            {
                FontSize = 20,
                MinWidth = 125,
                MaxWidth = 125,
                Margin = new Thickness(25, 0, 50, 50)
            };
            Button startQuiz = new Button
            {
                Content = "Zaczynamy!",
                FontSize = 20,
                MinWidth = 120,
                Margin = new Thickness(50, 0, 0, 0)
            };
            startQuiz.Click += new RoutedEventHandler(StartQuiz);
            Button goBack = new Button
            {
                Content = "Powrót",
                FontSize = 20,
                MinWidth = 100,
                Margin = new Thickness(50, 0, 22, 0)
            };
            goBack.Click += new RoutedEventHandler(GoBackToMainMenu);

            StackPanel buttonsPanel = new StackPanel
            {
                Orientation = 0
            };

            Title.Margin = new Thickness(0, 0, 0, 45);
            userAvatar.BeginAnimation(OpacityProperty, animation: fadeIn);
            userLabel.BeginAnimation(OpacityProperty, animation: fadeIn);
            enterUserName.BeginAnimation(OpacityProperty, animation: fadeIn);
            goBack.BeginAnimation(OpacityProperty, animation: fadeIn);
            startQuiz.BeginAnimation(OpacityProperty, animation: fadeIn);

            userNamePanel.Children.Add(userLabel);
            userNamePanel.Children.Add(enterUserName);
            buttonsPanel.Children.Add(goBack);
            buttonsPanel.Children.Add(startQuiz);
            userPhotoPanel.Children.Add(userAvatar);
            userPhotoPanel.Children.Add(userNamePanel);
            userPhotoPanel.Children.Add(buttonsPanel);

            MainPanel.Children.Remove(buttonPanel);
            MainPanel.Children.Add(userPhotoPanel);
        }

        private void GoBackToMainMenu(object sender, RoutedEventArgs e)
        {
            DoubleAnimation fadeOutAndDo2 = new DoubleAnimation(1.0, 0.0, new Duration(TimeSpan.FromMilliseconds(250)));
            fadeOutAndDo2.Completed += new EventHandler(ShowBack);
            bool once = true;
            foreach (FrameworkElement child in userPhotoPanel.Children)
            {
                if (child != null)
                {
                    if (once)
                    {
                        child.BeginAnimation(OpacityProperty, fadeOutAndDo2);
                        once = false;
                    }
                    else
                    {
                        child.BeginAnimation(OpacityProperty, fadeOut);
                    }

                }
            }
        }

        private void ShowBack(object sender, EventArgs e)
        {
            Title.Margin = new Thickness(0, 0, 0, 90);
            MainPanel.Children.Remove(userPhotoPanel);
            startButton.BeginAnimation(OpacityProperty, fadeIn);
            helpButton.BeginAnimation(OpacityProperty, fadeIn);
            resultButton.BeginAnimation(OpacityProperty, fadeIn);
            MainPanel.Children.Add(buttonPanel);
        }

        private void ChangePage(object sender, EventArgs e)
        {
            main.Content = new IntroductionPage(main, enterUserName.Text);
        }

        private void ShowResults(object sender, EventArgs e)
        {
            main.Content = new ResultsView(main);
        }

        private void ShowHelp(object sender, EventArgs e)
        {
            main.Content = new HelpPage(main);       
        }

    }
}
