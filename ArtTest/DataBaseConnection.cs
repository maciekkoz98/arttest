﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtTest
{
    class DataBaseConnection
    {
        public static List<User> GetPeople()
        {
            using (IDbConnection con = new SQLiteConnection(LoadConnectionString()))
            {
                var output = con.Query<User>("select * from users", new DynamicParameters());
                return output.ToList();
            }
        }

        public static void ResetUsers()
        {
            using (IDbConnection con = new SQLiteConnection(LoadConnectionString()))
            {
                con.Open();
                string query = "drop table if exists users";
                var com = con.CreateCommand();
                com.CommandText = query;
                com.ExecuteNonQuery();
                query = "create table if not exists users ([id] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, [username] TEXT NOT NULL, [score] INTEGER NOT NULL, [answers] TEXT NOT NULL)";
                com.CommandText = query;
                com.ExecuteNonQuery();
            }
        }

        public static void AddUser(User current)
        {
            using (IDbConnection con = new SQLiteConnection(LoadConnectionString()))
            {
                con.Open();
                using (var command = con.CreateCommand())
                {
                    command.CommandTimeout = 0;
                    command.CommandText = "INSERT INTO [users] (username, score, answers) VALUES (@username, @score, @answers)";
                    command.Parameters.Add(new SQLiteParameter("@username", current.UserName));
                    command.Parameters.Add(new SQLiteParameter("@score", current.Score));
                    command.Parameters.Add(new SQLiteParameter("@answers", current.GetAnswers()));
                    command.ExecuteNonQuery();
                }
            }
        }

        public static void DeleteUser(User current)
        {
            using (IDbConnection con = new SQLiteConnection(LoadConnectionString()))
            {
                con.Open();
                using(var command = con.CreateCommand())
                {
                    command.CommandTimeout = 0;
                    command.CommandText = "DELETE FROM USERS where id = @id";
                    command.Parameters.Add(new SQLiteParameter("@id", current.ID));
                    command.ExecuteNonQuery();
                }
            }
        }

        private static string LoadConnectionString(string id = "ResultsDB")
        {
            return ConfigurationManager.ConnectionStrings[id].ConnectionString;
        }
    }
}
