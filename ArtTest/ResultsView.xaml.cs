﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media.Animation;

namespace ArtTest
{
    /// <summary>
    /// Logika interakcji dla klasy ResultsView.xaml
    /// </summary>
    public partial class ResultsView : Page
    {
        public List<User> UserList { get; set; }

        private readonly Frame main;
        readonly DoubleAnimation fadeIn = new DoubleAnimation(0.0, 1.0, new Duration(TimeSpan.FromMilliseconds(250)));
        readonly DoubleAnimation fadeOut = new DoubleAnimation(1.0, 0.0, new Duration(TimeSpan.FromMilliseconds(250)));
        private User current;
        private bool animGuard = true;

        public ResultsView(Frame main)
        {
            InitializeComponent();
            UserList = DataBaseConnection.GetPeople();
            UserList.Sort(User.ScoreComparison);
            DataContext = this;
            this.main = main;
            FadeInEverything();
        }

        private void BackToMainMenu(object sender, EventArgs e)
        {
            DoubleAnimation fadeOut = new DoubleAnimation(1.0, 0.0, new Duration(TimeSpan.FromMilliseconds(250)));
            fadeOut.Completed += new EventHandler(ChangeToMainMenu);
            MainStackPanel.BeginAnimation(OpacityProperty, fadeOut);
        }

        private void ChangeToMainMenu(object sender, EventArgs e)
        {
            main.Content = new MainMenu(main);
        }

        private void FadeInEverything()
        {
            TitleStackPanel.BeginAnimation(OpacityProperty, fadeIn);
            ResetButton.BeginAnimation(OpacityProperty, fadeIn);
            DataBaseView.BeginAnimation(OpacityProperty, fadeIn);
            UserAvatar.BeginAnimation(OpacityProperty, fadeIn);
            UserNameLabel.BeginAnimation(OpacityProperty, fadeIn);
        }

        private void ChoseItem(object sender, EventArgs e)
        {
            if (sender is ListViewItem item)
            {
                if (animGuard)
                {
                    FadeInTheRest();
                }
                current = (User)item.DataContext;               
                UserNameLabel.BeginAnimation(OpacityProperty, fadeOut);
                PointsLabel.BeginAnimation(OpacityProperty, fadeOut);
                fadeOut.Completed += ChangeUserNameLabel;
                ResultsDescriptionLabel.BeginAnimation(OpacityProperty, fadeOut);
            }
        }

        private void ChangeUserNameLabel(object sender, EventArgs e)
        {
            UserNameLabel.Text = current.UserName;
            UserNameLabel.BeginAnimation(OpacityProperty, fadeIn);
            PointsLabel.Text = current.Score.ToString();
            PointsLabel.BeginAnimation(OpacityProperty, fadeIn);
            if (current.Score < 10)
            {
                ResultsDescriptionLabel.Text = "Poniżej średniej";
            }
            else if (current.Score >= 10 && current.Score <= 14)
            {
                ResultsDescriptionLabel.Text = "W granicach średniej";
            }
            else
            {
                ResultsDescriptionLabel.Text = "Powyżej średniej";
            }
            ResultsDescriptionLabel.BeginAnimation(OpacityProperty, fadeIn);
            fadeOut.Completed -= ChangeUserNameLabel;
        }

        private void FadeInTheRest()
        {
            PointsStackPanel.BeginAnimation(OpacityProperty, fadeIn);
            ResultsStackPanel.BeginAnimation(OpacityProperty, fadeIn);
            DeleteUserButton.BeginAnimation(OpacityProperty, fadeIn);
            animGuard = false;
        }

        private void DeleteUser(object sender, EventArgs e)
        {
            if (current != null)
            {
                DataBaseConnection.DeleteUser(current);
                UserList.Remove(current);
                ICollectionView view = CollectionViewSource.GetDefaultView(DataBaseView.ItemsSource);
                view.Refresh();
                PointsStackPanel.BeginAnimation(OpacityProperty, fadeOut);
                ResultsStackPanel.BeginAnimation(OpacityProperty, fadeOut);
                DeleteUserButton.BeginAnimation(OpacityProperty, fadeOut);
                animGuard = true;
                fadeOut.Completed += OriginalUserNameLabel;
                UserNameLabel.BeginAnimation(OpacityProperty, fadeOut);
            }
        }

        private void OriginalUserNameLabel(object sender, EventArgs e)
        {
            UserNameLabel.Text = "Wybierz osobę";
            UserNameLabel.BeginAnimation(OpacityProperty, fadeIn);
            fadeOut.Completed -= OriginalUserNameLabel;
        }

        private void ResetResultsTable(object sender, EventArgs e)
        {
            DataBaseConnection.ResetUsers();
            UserList.Clear();
            ICollectionView view = CollectionViewSource.GetDefaultView(DataBaseView.ItemsSource);
            view.Refresh();
        }
    }
}
