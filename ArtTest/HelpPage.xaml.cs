﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ArtTest
{
    /// <summary>
    /// Logika interakcji dla klasy HelpPage.xaml
    /// </summary>
    public partial class HelpPage : Page
    {
        private readonly Frame main;
        public HelpPage(Frame main)
        {
            this.main = main;
            InitializeComponent();
            DoubleAnimation fadeIn = new DoubleAnimation(0.0, 1.0, new Duration(TimeSpan.FromMilliseconds(250)));
            MainStack.BeginAnimation(OpacityProperty, fadeIn);
        }

        private void BackToMainMenu(object sender, EventArgs e)
        {
            DoubleAnimation fadeOut = new DoubleAnimation(1.0, 0.0, new Duration(TimeSpan.FromMilliseconds(250)));
            fadeOut.Completed += new EventHandler(ChangePage);
            MainStack.BeginAnimation(OpacityProperty, fadeOut);
           
        }

        private void ChangePage(object sender, EventArgs e)
        {
            main.Content = new MainMenu(main);
        }
    }
}
