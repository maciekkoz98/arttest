﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ArtTest
{
    /// <summary>
    /// Logika interakcji dla klasy IntroductionStage.xaml
    /// </summary>
    public partial class IntroductionPage : Page
    {
        private readonly Frame main;
        private readonly string userName;

        public IntroductionPage(Frame main, string userName)
        {
            InitializeComponent();
            this.main = main;
            this.userName = userName;
        }

        public void TriggerText1(object sender, EventArgs e)
        {
            DoubleAnimation fadeIn = new DoubleAnimation(0.0, 1.0, new Duration(TimeSpan.FromMilliseconds(1000)));
            fadeIn.Completed += new EventHandler(TriggerText2);
            Text1.BeginAnimation(OpacityProperty, fadeIn);
        }

        private void TriggerText2(object sender, EventArgs e)
        {
            DoubleAnimation fadeIn = new DoubleAnimation(0.0, 1.0, new Duration(TimeSpan.FromMilliseconds(1000)));
            fadeIn.Completed += new EventHandler(ShowButton);
            Text2.BeginAnimation(OpacityProperty, fadeIn);
        }

        private void ShowButton(object sender, EventArgs e)
        {
            DoubleAnimation fadeIn = new DoubleAnimation(0.0, 1.0, new Duration(TimeSpan.FromMilliseconds(900)));           
            StartButton.IsEnabled = true;
            StartButton.BeginAnimation(OpacityProperty, fadeIn);
        }

        private void BeginQuiz(object sender, RoutedEventArgs e)
        {
            DoubleAnimation fadeOut = new DoubleAnimation(1.0, 0.0, new Duration(TimeSpan.FromMilliseconds(250)));           
            Title.BeginAnimation(OpacityProperty, fadeOut);
            Text1.BeginAnimation(OpacityProperty, fadeOut);
            Text2.BeginAnimation(OpacityProperty, fadeOut);
            fadeOut.Completed += new EventHandler(ChangePageToQuiz);
            StartButton.BeginAnimation(OpacityProperty, fadeOut);         
        }

        private void ChangePageToQuiz(object sender, EventArgs e)
        {
            main.Content = new QuestionPage(main, userName);
        }
    }
}
