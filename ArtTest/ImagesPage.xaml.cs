﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ArtTest
{
    /// <summary>
    /// Logika interakcji dla klasy ImagesPage.xaml
    /// </summary>
    public partial class ImagesPage : Page
    {
        private readonly QuestionPage questionPage;
        private string buttonClicked = null;

        public ImagesPage(int imageNumber, bool isLeft, QuestionPage parent)
        {
            InitializeComponent();
            questionPage = parent;
            string label = imageNumber.ToString();
            aImage.Source = new BitmapImage(new Uri("pack://application:,,,/Images/" + label + "A.png"));
            bImage.Source = new BitmapImage(new Uri("pack://application:,,,/Images/" + label + "B.png"));
            cImage.Source = new BitmapImage(new Uri("pack://application:,,,/Images/" + label + "C.png"));

            if (isLeft)
            {
                imagesPanel.Margin = new Thickness(950, 50, 0, 0);
            }
            else
            {
                imagesPanel.Margin = new Thickness(-950, 50, 0, 0);
            }       
        }

        public ImagesPage(QuestionPage parent)
        {
            InitializeComponent();
            questionPage = parent;
        }

        private void ClickedA(object sender, RoutedEventArgs e)
        {
            if (buttonClicked != null)
            {
                CloseAnswerLabel(buttonClicked);
            }
            ShowAnswerLabel("a");
            questionPage.NextActive();
        }

        private void ClickedB(object sender, RoutedEventArgs e)
        {
            if (buttonClicked != null)
            {
                CloseAnswerLabel(buttonClicked);
            }
            ShowAnswerLabel("b");
            questionPage.NextActive();
        }

        private void ClickedC(object sender, RoutedEventArgs e)
        {
            if (buttonClicked != null)
            {
                CloseAnswerLabel(buttonClicked);
            }
            ShowAnswerLabel("c");
            questionPage.NextActive();
        }

        public void ShowAnswerLabel(string button)
        {
            SetStoryboard("labelUp", button);
            buttonClicked = button;
        }

        private void CloseAnswerLabel(string button)
        {
            SetStoryboard("labelDown", button);
            buttonClicked = null;
        }

        public void SetStoryboard(string storyName, string button)
        {
            Storyboard labelAn = Resources[storyName] as Storyboard;
            switch (button)
            {
                case "a":
                    labelAn.SetValue(Storyboard.TargetNameProperty, "aLabel");
                    break;
                case "b":
                    labelAn.SetValue(Storyboard.TargetNameProperty, "bLabel");
                    break;
                case "c":
                    labelAn.SetValue(Storyboard.TargetNameProperty, "cLabel");
                    break;
                default:
                    return;
            }
            labelAn.Begin();
        }

        public void SetAnswerLabelUp(string ansLabel)
        {
            switch (ansLabel)
            {
                case "a":
                    aLabel.Margin = new Thickness(89.5, 0, 0, 0);
                    break;
                case "b":
                    bLabel.Margin = new Thickness(89.5, 0, 0, 0);
                    break;
                case "c":
                    cLabel.Margin = new Thickness(89.5, 0, 0, 0);
                    break;
            }
        }

        public string GetButtonClicked() => buttonClicked;
    }
}
