﻿#pragma checksum "..\..\ResultsView.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "75B1D513A9FB0BE9878D78CBCD2D4B7D2A6841CF1DD5D5A820D80F6F3E9AA300"
//------------------------------------------------------------------------------
// <auto-generated>
//     Ten kod został wygenerowany przez narzędzie.
//     Wersja wykonawcza:4.0.30319.42000
//
//     Zmiany w tym pliku mogą spowodować nieprawidłowe zachowanie i zostaną utracone, jeśli
//     kod zostanie ponownie wygenerowany.
// </auto-generated>
//------------------------------------------------------------------------------

using ArtTest;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace ArtTest {
    
    
    /// <summary>
    /// ResultsView
    /// </summary>
    public partial class ResultsView : System.Windows.Controls.Page, System.Windows.Markup.IComponentConnector, System.Windows.Markup.IStyleConnector {
        
        
        #line 13 "..\..\ResultsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel MainStackPanel;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\ResultsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel TitleStackPanel;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\ResultsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ResetButton;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\ResultsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView DataBaseView;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\ResultsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel UserDetails;
        
        #line default
        #line hidden
        
        
        #line 74 "..\..\ResultsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image UserAvatar;
        
        #line default
        #line hidden
        
        
        #line 80 "..\..\ResultsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock UserNameLabel;
        
        #line default
        #line hidden
        
        
        #line 85 "..\..\ResultsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel PointsStackPanel;
        
        #line default
        #line hidden
        
        
        #line 90 "..\..\ResultsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock PointsNameLabel;
        
        #line default
        #line hidden
        
        
        #line 94 "..\..\ResultsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock PointsLabel;
        
        #line default
        #line hidden
        
        
        #line 102 "..\..\ResultsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel ResultsStackPanel;
        
        #line default
        #line hidden
        
        
        #line 106 "..\..\ResultsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock ResultsNameLabel;
        
        #line default
        #line hidden
        
        
        #line 111 "..\..\ResultsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock ResultsDescriptionLabel;
        
        #line default
        #line hidden
        
        
        #line 117 "..\..\ResultsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button DeleteUserButton;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/ArtTest;component/resultsview.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\ResultsView.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.MainStackPanel = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 2:
            this.TitleStackPanel = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 3:
            
            #line 22 "..\..\ResultsView.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.BackToMainMenu);
            
            #line default
            #line hidden
            return;
            case 4:
            this.ResetButton = ((System.Windows.Controls.Button)(target));
            
            #line 39 "..\..\ResultsView.xaml"
            this.ResetButton.Click += new System.Windows.RoutedEventHandler(this.ResetResultsTable);
            
            #line default
            #line hidden
            return;
            case 5:
            this.DataBaseView = ((System.Windows.Controls.ListView)(target));
            return;
            case 7:
            this.UserDetails = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 8:
            this.UserAvatar = ((System.Windows.Controls.Image)(target));
            return;
            case 9:
            this.UserNameLabel = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 10:
            this.PointsStackPanel = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 11:
            this.PointsNameLabel = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 12:
            this.PointsLabel = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 13:
            this.ResultsStackPanel = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 14:
            this.ResultsNameLabel = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 15:
            this.ResultsDescriptionLabel = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 16:
            this.DeleteUserButton = ((System.Windows.Controls.Button)(target));
            
            #line 119 "..\..\ResultsView.xaml"
            this.DeleteUserButton.Click += new System.Windows.RoutedEventHandler(this.DeleteUser);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        void System.Windows.Markup.IStyleConnector.Connect(int connectionId, object target) {
            System.Windows.EventSetter eventSetter;
            switch (connectionId)
            {
            case 6:
            eventSetter = new System.Windows.EventSetter();
            eventSetter.Event = System.Windows.UIElement.PreviewMouseLeftButtonDownEvent;
            
            #line 64 "..\..\ResultsView.xaml"
            eventSetter.Handler = new System.Windows.Input.MouseButtonEventHandler(this.ChoseItem);
            
            #line default
            #line hidden
            ((System.Windows.Style)(target)).Setters.Add(eventSetter);
            break;
            }
        }
    }
}

