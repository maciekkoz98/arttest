﻿#pragma checksum "..\..\SumUpPage.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "D512E3FAD7AD57EFA9295F12F7237151B6C5761129FE8BF0380AC77FD7968139"
//------------------------------------------------------------------------------
// <auto-generated>
//     Ten kod został wygenerowany przez narzędzie.
//     Wersja wykonawcza:4.0.30319.42000
//
//     Zmiany w tym pliku mogą spowodować nieprawidłowe zachowanie i zostaną utracone, jeśli
//     kod zostanie ponownie wygenerowany.
// </auto-generated>
//------------------------------------------------------------------------------

using ArtTest;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace ArtTest {
    
    
    /// <summary>
    /// SumUpPage
    /// </summary>
    public partial class SumUpPage : System.Windows.Controls.Page, System.Windows.Markup.IComponentConnector {
        
        
        #line 13 "..\..\SumUpPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel MainStack;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\SumUpPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock TitleLabel;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\SumUpPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel TableLabelStack;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\SumUpPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel StuffStack;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\SumUpPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock UserNameLabel;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\SumUpPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock PointsLabel;
        
        #line default
        #line hidden
        
        
        #line 54 "..\..\SumUpPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel ResultsStackPanel;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\SumUpPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock ResultsNameLabel;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\SumUpPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock ResultsDescriptionLabel;
        
        #line default
        #line hidden
        
        
        #line 72 "..\..\SumUpPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid Grid;
        
        #line default
        #line hidden
        
        
        #line 123 "..\..\SumUpPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image Ans1;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/ArtTest;component/sumuppage.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\SumUpPage.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.MainStack = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 2:
            this.TitleLabel = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 3:
            this.TableLabelStack = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 4:
            this.StuffStack = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 5:
            this.UserNameLabel = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 6:
            this.PointsLabel = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 7:
            this.ResultsStackPanel = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 8:
            this.ResultsNameLabel = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 9:
            this.ResultsDescriptionLabel = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 10:
            this.Grid = ((System.Windows.Controls.Grid)(target));
            return;
            case 11:
            this.Ans1 = ((System.Windows.Controls.Image)(target));
            return;
            case 12:
            
            #line 601 "..\..\SumUpPage.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.EndTest);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

