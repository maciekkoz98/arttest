﻿using System;
using System.Windows;

namespace ArtTest
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void LoadMainMenu(object sender, RoutedEventArgs e)
        {
            main.Content = new MainMenu(main);
        }
    }
}
