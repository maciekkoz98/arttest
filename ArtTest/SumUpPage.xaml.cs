﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ArtTest
{
    /// <summary>
    /// Logika interakcji dla klasy SumUpPage.xaml
    /// </summary>
    public partial class SumUpPage : Page
    {
        private readonly Frame main;
        private readonly User current;

        public SumUpPage(Frame main, User current)
        {
            this.current = current;
            this.main = main;
            InitializeComponent();
            SetUserElements();
        }

        private void SetUserElements()
        {
            UserNameLabel.Text = current.UserName;
            PointsLabel.Text = current.Score.ToString();
            if (current.Score < 10)
            {
                ResultsDescriptionLabel.Text = "poniżej średniej";
            }
            else if (current.Score >= 10 && current.Score <= 14)
            {
                ResultsDescriptionLabel.Text = "w średniej";
            }
            else
            {
                ResultsDescriptionLabel.Text = "powyżej średniej";
            }
            SetAnswersImages();
        }

        private void SetAnswersImages()
        {
            int ansID = 1;
            for (int i = 0; i < Grid.Children.Count; i++)
            {                 
                UIElement cur = Grid.Children[i];
                if (cur is Image && Grid.GetColumn(cur) == 0)
                {
                    Image ans = (Image)cur;
                    ans.Source = new BitmapImage(new Uri("pack://application:,,,/Images/"+ansID.ToString()+current.GetAnswer(ansID-1).ToString()+".png"));
                    ansID++;
                }
            }
        }       

        private void EndTest(object sender, EventArgs e)
        {           
            current.Export();
            DoubleAnimation fadeOut = new DoubleAnimation(1.0, 0.0, new Duration(TimeSpan.FromMilliseconds(250)));
            fadeOut.Completed += new EventHandler(ChangeFrame);
            MainStack.BeginAnimation(OpacityProperty, fadeOut);
        }

        private void ChangeFrame(object sender, EventArgs e)
        {
            main.Content = new MainMenu(main);
        }
    }
}
